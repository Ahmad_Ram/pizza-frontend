/* eslint-disable no-script-url */

import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import axios from "axios";
import * as moment from 'moment'
import { Link as RouterLink } from 'react-router-dom';


const useStyles = makeStyles(theme => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

class Orders extends React.Component {
  constructor(props) {
      super(props);
      this.classes = this.props.classes;
      this.state = {
          orders: []
      };
  }
  componentDidMount() {
        axios
            .get("/api/admin/orders/get")
            .then(res => {
              console.log(res.data);
                this.setState({
                    orders: res.data.map(x => {
                        return { ...x };
                    })
                });
            })
            .catch(error => {});
    }

  render() {
    return (
    <React.Fragment>
      <Title>Recent Orders</Title>
      <Table size="medium">
        <TableHead>
          <TableRow>
            <TableCell>Id</TableCell>
            <TableCell>Customer</TableCell>
            <TableCell>Number of Pizza</TableCell>
            <TableCell>Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {this.state.orders.reverse().map(row => (
            <TableRow key={row.id}>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.user.firstname}</TableCell>
              <TableCell>{row.pizzas.length}</TableCell>
              <TableCell>{moment(row.created_at).format("LLLL")}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}
}
export default (Orders);
