import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CameraIcon from "@material-ui/icons/PhotoCamera";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Link from "@material-ui/core/Link";
import SharedStorage from "./SharedStorage";

import PizzaCard from "./PizzaCard";
import axios from "axios";

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © Ahmad Ram "}
        {" "}
        {new Date().getFullYear()}
        {". "}
        <Link color="inherit" href="https://bitbucket.org/Ahmad_Ram">
            Bitbucket Repositories
        </Link>
        </Typography>
    );
}

const useStyles = theme => ({
    icon: {
        marginRight: theme.spacing(2)
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6)
    },
    heroButtons: {
        marginTop: theme.spacing(4)
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column"
    },
    cardMedia: {
        paddingTop: "56.25%" // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6)
    }
});

class Pizza extends React.Component {
    constructor(props) {
        super(props);
        this.classes = this.props.classes;
        this.state = {
            pizza: [],
            order: [],
            isLoading: false
        };
    }
    componentDidMount() {
        axios
            .get("/api/pizza/get")
            .then(res => {
                this.setState({
                    pizza: res.data.data.map(x => {
                        return { ...x, checked: false };
                    })
                });
            })
            .catch(error => {});
    }

    resetOrder() {
        this.setState({
            pizza: this.state.pizza.map(x => {
                return { ...x, checked: false };
            }),
            order: []
        });
    }
    handleCheckChange(data) {
        let pizza = this.state.pizza.map(x => {
            if (data.id == x.id) x.checked = data.checked;
            return x;
        });
        var order = [];
        if (data.checked) {
            order = this.state.order.concat(data.id);
        } else {
            order = this.state.order.filter(x => x != data.id);
        }
        this.setState({
            order: order,
            pizza: pizza
        });
    }
    submitOrder() {
        if (SharedStorage.isLogedIn()) {
            if (this.state.order.length > 0){
                this.setState({ isLoading: true });
                axios
                    .post("/api/orders/store", { ids: this.state.order })
                    .then(res => {
                        this.props.showMessage(res.data.message);
                        this.resetOrder();
                        this.setState({ isLoading: false });
                    })
                    .catch(error => {
                        alert("error" + JSON.stringify(error));
                        this.setState({ isLoading: false });
                    });
                  }
            else
                this.props.showMessage("Please select some pizza to add to the order");
        } 
        else this.props.showMessage("Please signin first");
    }
    render() {
        var classes = this.classes;
        return (
            <React.Fragment>
                <CssBaseline />
                <main>
                    {/* Hero unit */}
                    <div className={classes.heroContent}>
                        <Container maxWidth="sm">
                            <Typography
                                component="h1"
                                variant="h5"
                                align="center"
                                color="textPrimary"
                                gutterBottom
                            >
                                Welcome to our restaurant
                            </Typography>
                        </Container>
                    </div>
                    <Container className={classes.cardGrid} maxWidth="md">
                        {/* End hero unit */}
                        <Grid container spacing={2}>
                            {this.state.pizza.map(card => (
                                <PizzaCard
                                    name={card.name}
                                    photo={card.photo}
                                    id={card.id}
                                    checked={card.checked}
                                    onChange={data =>
                                        this.handleCheckChange(data)
                                    }
                                    description={card.description}
                                />
                            ))}
                        </Grid>
                        <div
                            style={{
                                "text-align": "right",
                                "padding-top": "5px"
                            }}
                        >
                            <Button
                                disabled={ this.state.isLoading }
                                onClick={() => this.submitOrder()}
                                color="primary"
                            >
                                Submit Order
                            </Button>
                        </div>
                    </Container>
                </main>
                {/* Footer */}
                <footer className={classes.footer}>
                    <Typography variant="h6" align="center" gutterBottom>
                        Ahmad Ram
                    </Typography>
                    <Copyright />
                </footer>
                {/* End footer */}
            </React.Fragment>
        );
    }
}
export default withStyles(useStyles)(Pizza);
