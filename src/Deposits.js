/* eslint-disable no-script-url */

import React, { useState } from "react";
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';
import axios from "axios";

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});


class Deposits extends React.Component {

    constructor(props) {
      super(props);
      this.classes = this.props.classes;
      this.state = {
          orders: []
      };
    }

    componentDidMount() {
          axios
              .get("/api/admin/orders/get")
              .then(res => {
                console.log(res.data);
                  this.setState({
                      orders: res.data.map(x => {
                          return { ...x };
                      })
                  });
              })
              .catch(error => {});
      }

    render() {
  return (
    <React.Fragment>
      <Title>Total</Title><br />
      <Typography component="p" variant="h4">
        {this.state.orders.length} Orders
      </Typography><br /><br />
      <Typography color="textSecondary">
        Till {new Date().getDate()}/{new Date().getMonth()+1}/{new Date().getFullYear()}
      </Typography>
    </React.Fragment>
  );
}
}
export default (Deposits);
