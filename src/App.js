import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Link2 from "@material-ui/core/Link";
import ProTip from "./ProTip";
import Button from "@material-ui/core/Button";
import SignUp from "./SignUp";
import Signin from "./Signin";
import Dashboard from "./Dashboard";
import Previousorders from "./PreviousOrders";
import Details from "./Details";
import SharedStorage from "./SharedStorage";
import HistoryIcon from '@material-ui/icons/History';
import axios from "axios";
import Pizza from "./Pizza";
import {
    Route,
    Link,
    BrowserRouter as Router,
    Redirect
} from "react-router-dom";
import {
    AppBar,
    Snackbar,
    Toolbar,
    makeStyles,
    IconButton
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    }
}));

const componentStyle = {
  container: {
    display: 'flex',
    width: '100%',
    height: '100vh',
    backgroundColor: '#212121',
  },
  iconBack: {
    cursor: 'crosshair'
  },
  eventHeadingBack: {
    color: '#fff',
    marginRight: '16px'
  }
}

function ButtonAppBar(props) {
    const classes = useStyles();
    const [count, setCount] = useState(false);
    const logout = history => {
        setCount(true);
        axios
            .post("/api/auth/logout")
            .then(res => {
                SharedStorage.resetUserInfo();
                props.showMessage(res.data.message);
                setCount(false);
            })
            .catch(error => {
                SharedStorage.resetUserInfo();
                props.showMessage("Network error");
                setCount(false);
            });
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                <Route
                    render={({ history }) => (

                    <Typography variant="h6" className={classes.title} style={{cursor: 'pointer'}}
                    onClick={() => history.push("/")}>
                        Pizza
                    </Typography>
                  )}
              />

                    <Route
                        render={({ history }) => (
                    <Button
                    onClick={() => history.push("/previousorders")}
                    color="inherit"
                    >
                    My Previous Order
                    <HistoryIcon>

                        </HistoryIcon>

                    </Button>
                  )}
              />

                    {SharedStorage.isLogedIn() ? (
                        <Route
                            render={({ history }) => (
                                <Button
                                    onClick={() => logout(history)}
                                    color="inherit"
                                    disabled={ count }
                                >
                                    Logout
                                </Button>
                            )}
                        />
                    ) : (
                        <Route
                            render={({ history }) => (
                                <Button
                                    onClick={() => history.push("/signin")}
                                    color="inherit"
                                >
                                    Login
                                </Button>
                            )}
                        />
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © Ahmad Ram "}
        {" "}
        {new Date().getFullYear()}
        {". "}
        <Link color="inherit" href="https://bitbucket.org/Ahmad_Ram">
            Bitbucket Repositories
        </Link>
        </Typography>
    );
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            message: "message"
        };
    }
    handleClose() {
        this.setState({ open: false });
    }
    showMessage = text => {
        this.setState({ open: true, message: text });
    };
    render() {
        return (
            <div>

                {SharedStorage.isAdmin() ? (
                <ButtonAppBar showMessage={this.showMessage} />
              ):
              <Redirect
                  to={{
                      pathname: "/dashboard"
                  }}
              />
               }

                {SharedStorage.isLogedIn() ? (
                    <Route
                        exact path="/"
                        render={props => (
                            <Pizza {...props} showMessage={this.showMessage} />
                        )}
                    />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/signin"
                        }}
                    />
                )}

                <Route
                    exact path="/signup"
                    render={props => (
                        SharedStorage.isLogedIn() ? SharedStorage.isAdmin() ? <Redirect to={{pathname: "/"}} /> : <Redirect to={{pathname: "/dashboard"}} /> :
                        <SignUp {...props} showMessage={this.showMessage} />
                    )}
                />

                <Route
                    exact path="/signin"
                    render={props => (
                        SharedStorage.isLogedIn() ? SharedStorage.isAdmin() ? <Redirect to={{pathname: "/"}} /> : <Redirect to={{pathname: "/dashboard"}} /> :
                        <Signin {...props} showMessage={this.showMessage} />
                    )}
                />

                <Route
                    exact path="/dashboard"
                    render={props => (
                        SharedStorage.isAdmin() ? <Redirect to={{pathname: "/"}} /> :
                        <Dashboard {...props} showMessage={this.showMessage} />
                    )}

                />

                <Route
                    path="/previousorders"
                    render={props => (
                        <Previousorders/>
                    )}
                />

                <Route
                state={this.state}
                exact path="/details/:id"
                render={(props) => <Details {...props} state={this.state} />}
                 />


                <Snackbar
                    open={this.state.open}
                    autoHideDuration={3000}
                    onClose={() => this.handleClose()}
                    ContentProps={{
                        "aria-describedby": "message-id"
                    }}
                    message={<span id="message-id">{this.state.message}</span>}
                />
            </div>
        );
    }
}
