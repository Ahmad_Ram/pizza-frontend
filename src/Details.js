import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from "@material-ui/core/Typography";
import { Link as RouterLink } from 'react-router-dom';
import axios from "axios";
import * as moment from 'moment'
import { CardMedia } from "@material-ui/core";



class Details extends React.Component {
  constructor(props) {
      super(props);
      this.classes = this.props.classes;
      this.state = {
          id: 0,
          date: "",
          orders: [],
          counter: 0
      };
  }

  componentDidMount() {
      axios
          .get("/api/orders/get/"+ this.props.match.params.id)
          .then(res => {
              this.setState({ id: res.data.data.id });
              this.setState({ date: res.data.data.created_at });
              this.setState({ orders: res.data.data.pizzas });
          })
          .catch(error => {});
  }

  render() {
  return (
    <React.Fragment>
    ~{"\n"}

    <Typography
        component="h1"
        variant="h4"
        align="center"
        color="primary"
        m="auto"
        gutterBottom
    >
        Order Details
    </Typography>

    <Table size="medium">
      <TableHead>
        <TableRow>
          <TableCell align="center">
          <Typography
              component="h3"
              align="left"
              color="black"
              style={{ fontSize: '20px', fontWeight: 'bold' }}
              m="auto"
              gutterBottom
          >
              Order No. {this.state.id}
          </Typography>
          </TableCell>
          <TableCell align="center">
          <Typography
              component="h3"
              align="right"
              color="black"
              style={{ fontSize: '20px', fontWeight: 'bold' }}
              m="auto"
              gutterBottom
          >
              Order Date: {moment(this.state.date).format("LLL")}
          </Typography>
          </TableCell>
        </TableRow>
      </TableHead>
    </Table>

      <Table size="medium">
        <TableHead>
          <TableRow>
            <TableCell align="center">#</TableCell>
            <TableCell align="center">Pizza Name</TableCell>
            <TableCell align="center">Description</TableCell>
            <TableCell align="center">Image</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.orders.reverse().map((row,index) => (
            <TableRow key={row.id}>
               <TableCell align="center">{index+1}</TableCell>
              <TableCell align="center">{row.name}</TableCell>
              <TableCell align="center">{row.description}</TableCell>
              <TableCell align="center" style = {{ width: "25%" }}>
              <CardMedia style = {{ paddingTop: "56.25%" }}
                  image={row.photo}
                  title={row.name}
              />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

    </React.Fragment>
  );
}
}
export default (Details);
