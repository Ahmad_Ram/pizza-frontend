import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from "@material-ui/core/Typography";
import { Link as RouterLink } from 'react-router-dom';
import axios from "axios";
import * as moment from 'moment'



class PreviousOrders extends React.Component {
  constructor(props) {
      super(props);
      this.classes = this.props.classes;
      this.state = {
          orders: []
      };
  }

  componentDidMount() {
      axios
          .get("/api/orders/get")
          .then(res => {
              this.setState({
                  orders: res.data.data.map(x => {
                      return { ...x };
                  })
              });
          })
          .catch(error => {});
  }

  render() {
  return (
    <React.Fragment>
    ~{"\n"}
    <Typography
        component="h1"
        variant="h5"
        align="center"
        color="primary"
        m="auto"
        gutterBottom
    >
        My Previos Orders
    </Typography>

      <Table size="medium">
        <TableHead>
          <TableRow>
            <TableCell align="center">#</TableCell>
            <TableCell align="center">Number of Pizza</TableCell>
            <TableCell align="center">Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.orders.reverse().map(row => (
            <TableRow key={row.id}>
            <TableCell align="center"><Link component={RouterLink} to={ '/details/' + row.id }>{row.id}</Link></TableCell>
              <TableCell align="center">{row.pizzas.length}</TableCell>
              <TableCell align="center">{moment(row.created_at).format("LLLL")}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

    </React.Fragment>
  );
}
}
export default (PreviousOrders);
