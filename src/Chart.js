import React from 'react';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import axios from "axios";
import * as moment from 'moment'

// Generate Sales Data
function createData(time, amount) {
  return { time, amount };
}

const data = [
  createData('1/1/2012', 0),
  createData('1/1/2012', 300),
  createData('1/1/2012', 600),
  createData('1/1/2012', 800),
  createData('1/1/2012', 1500),
  createData('1/1/2012', 2000),
  createData('1/1/2012', 2400),
  createData('1/1/2012', 2400),
  createData('1/1/2012', undefined),
];

class Chart extends React.Component {

    constructor(props) {
      super(props);
      this.classes = this.props.classes;
      this.state = {
          orders: []
      };
    }

    componentDidMount() {
          axios
              .get("/api/admin/orders/get")
              .then(res => {
                  this.setState({
                      orders: res.data.reverse().map(x => {
                          return { ...createData(moment(x.created_at).format("LT"), x.pizzas.length) };
                      })
                  });
              })
              .catch(error => {});
      }

    render() {
  return (
    <React.Fragment>
      <Title>Latest Orders</Title>
      <ResponsiveContainer>
        <LineChart
          data={this.state.orders}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="time" />
          <YAxis>
            <Label angle={270} position="left" style={{ textAnchor: 'middle' }}>
              Sales ($)
            </Label>
          </YAxis>
          <Line type="monotone" dataKey="amount" stroke="#556CD6" dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
}
export default (Chart);
