import axios from "axios";
class SharedStorage {
    isLogedIn() {
        return this.getUser() != null;
    }
    isAdmin() {
        if(this.getUser() == null) return false;
        return this.getUser().role != "employee";
    }
    getUser() {
        let value = localStorage.getItem("user");
        return value && JSON.parse(value);
    }
    resetUserInfo() {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
    }
    setUser(user) {
        let value = JSON.stringify(user);
        localStorage.setItem("user", value);
    }
    getToken() {
        let value = localStorage.getItem("token");
        return value && JSON.parse(value);
    }
    setToken(value) {
        localStorage.setItem("token", value);
        axios.defaults.headers.common["Authorization"] = `Bearer ${value}`;
    }
}
export default new SharedStorage();
