import axios from "axios";
import SharedStorage from "./SharedStorage";

axios.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
    "token"
)}`;

axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response.status !== 401) {
            return Promise.reject(error);
        }

        const tokens = {
            token: localStorage.getItem("token")
        };

        return axios
            .post(`api/auth/refresh`, tokens)
            .then(response => {
                localStorage.setItem("token", response.data.token);
                SharedStorage.setUser(response.data);
                axios.defaults.headers.common["Authorization"] = `Bearer ${
                    response.data.token
                }`;

                error.hasRefreshedToken = true;
                return Promise.reject(error);
            })
            .catch(() => {
                const tokenError = new Error("Cannot refresh token");
                tokenError.originalError = error;
                return Promise.reject(tokenError);
            });
    }
);
