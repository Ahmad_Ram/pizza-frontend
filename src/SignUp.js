import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { Link as RouterLink } from "react-router-dom";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import PropTypes from "prop-types";
import axios from "axios";

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © Ahmad Ram "}
        {" "}
        {new Date().getFullYear()}
        {". "}
        <Link color="inherit" href="https://bitbucket.org/Ahmad_Ram">
            Bitbucket Repositories
        </Link>
        </Typography>
    );
}

var useStyles = theme => ({
    body: {
        backgroundColor: theme.palette.common.white
    },
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
});

class Signup extends React.Component {
    constructor(props) {
        super(props);
        const { classes } = props;
        this.classes = classes;
        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            password: "",
            isLoading: false
        };
    }
    signup() {
        this.setState({ isLoading: true });
        axios
            .post("/api/auth/signup", this.state)
            .then(res => {
                this.props.history.push("/signin");
                this.props.showMessage("Your account has been created successfully!!");
                this.setState({ isLoading: false });
            })
            .catch(error => {
              this.props.showMessage(error.response.data.message[0].message);
              this.setState({ isLoading: false });
            });
    }
    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={this.classes.paper}>
                    <Avatar className={this.classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <form className={this.classes.form} noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    value={this.state.firstname}
                                    onChange={event =>
                                        this.setState({
                                            firstname: event.target.value
                                        })
                                    }
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="lname"
                                    value={this.state.lastname}
                                    onChange={event =>
                                        this.setState({
                                            lastname: event.target.value
                                        })
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    value={this.state.email}
                                    onChange={event =>
                                        this.setState({
                                            email: event.target.value
                                        })
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    value={this.state.password}
                                    onChange={event =>
                                        this.setState({
                                            password: event.target.value
                                        })
                                    }
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={this.classes.submit}
                            disabled={!this.state.lastname || !this.state.email || !this.state.password || this.state.isLoading }
                            onClick={() => this.signup()}
                        >
                            Sign Up
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link
                                    component={React.forwardRef(
                                        (props, ref) => (
                                            <RouterLink
                                                innerRef={ref}
                                                to="/signin"
                                                {...props}
                                            />
                                        )
                                    )}
                                    to="/signin"
                                    variant="body2"
                                >
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Container>
        );
    }
}

Signup.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(useStyles)(Signup);
