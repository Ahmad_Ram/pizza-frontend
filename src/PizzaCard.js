import { withStyles } from "@material-ui/core";
import React from "react";

import {
    Typography,
    CardContent,
    Card,
    CardMedia,
    Grid,
    CardActions,
    Button,
    FormControlLabel,
    Checkbox
} from "@material-ui/core";

const styles = theme => ({
    icon: {
        marginRight: theme.spacing(2)
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6)
    },
    heroButtons: {
        marginTop: theme.spacing(4)
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column"
    },
    cardMedia: {
        paddingTop: "56.25%" // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6)
    }
});
class PizzaCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 1,
            name: "Pizza",
            description: "",
            photo: "",
            ...props
        };

        this.classes = props.classes;
    }

    checkedChange(event) {
        if (this.props.onChange)
            this.props.onChange({
                id: this.state.id,
                checked: event.target.checked
            });
    }

    render() {
        var classes = this.classes;
        return (
            <Grid item key={this.state.id} xs={12} sm={12} md={6}>
                <Card className={classes.card}>
                    <CardMedia
                        className={classes.cardMedia}
                        image={this.state.photo}
                        title="Pizza"
                    />
                    <CardContent className={classes.cardContent}>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.state.name}
                        </Typography>
                        <Typography>{this.state.description}</Typography>
                    </CardContent>
                    <CardActions>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={this.props.checked}
                                    onChange={event =>
                                        this.checkedChange(event)
                                    }
                                    color="primary"
                                    inputProps={{
                                        "aria-label": "secondary checkbox"
                                    }}
                                />
                            }
                            label="Add"
                        />
                    </CardActions>
                </Card>
            </Grid>
        );
    }
}

export default withStyles(styles)(PizzaCard);
