import React from "react";
import SharedStorage from "./SharedStorage";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Snackbar from "@material-ui/core/Snackbar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import axios from "axios";
import PropTypes from "prop-types";
var useStyles = theme => ({
    root: {
        height: "100vh"
    },
    image: {
        backgroundImage:
            "url(https://images.unsplash.com/photo-1541745537411-b8046dc6d66c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center"
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
});

class SignInSide extends React.Component {
    constructor(props) {
        super(props);
        const { classes } = props;
        this.classes = classes;
        this.state = {
            password: "",
            email: "",
            open: false,
            isLoading: false
        };
    }

    Copyright() {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                {"Copyright © Ahmad Ram "}
                {" "}
                {new Date().getFullYear()}
                {". "}
                <Link color="inherit" href="https://bitbucket.org/Ahmad_Ram">
                    Bitbucket Repositories
                </Link>
            </Typography>
        );
    }
    login() {
      this.setState({ isLoading: true });
              axios
            .post("/api/auth/login", this.state)
            .then(res => {
                SharedStorage.setToken(res.data.data.token);
                SharedStorage.setUser(res.data.data);
                {SharedStorage.isAdmin() ? (this.props.history.push("/")):(this.props.history.push("/dashboard"))}
                this.props.showMessage("Logged in successfully.");
                this.setState({ isLoading: false });
            })
            .catch(error => {
              this.props.showMessage("Incorrect email address and/or password.");
              this.setState({ isLoading: false });
            });
    }
    render() {
        return (
            <Grid container component="main" className={this.classes.root}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    className={this.classes.image}
                />
                <Grid
                    item
                    xs={12}
                    sm={8}
                    md={5}
                    component={Paper}
                    elevation={6}
                    square
                >
                    <div className={this.classes.paper}>
                        <Avatar className={this.classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <form className={this.classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                value={this.state.email}
                                onChange={event =>
                                    this.setState({
                                        email: event.target.value
                                    })
                                }
                                autoComplete="email"
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                value={this.state.password}
                                onChange={event =>
                                    this.setState({
                                        password: event.target.value
                                    })
                                }
                                type="password"
                                id="password"
                                autoComplete="current-password"
                            />
                            <Button
                                type="button"
                                fullWidth
                                variant="contained"
                                color="primary"
                                disabled={!this.state.email || !this.state.password || this.state.isLoading }
                                className={this.classes.submit}
                                onClick={() => this.login()}
                            >
                                Sign In
                            </Button>
                            <Grid container>
                                <Grid item>
                                    <Link
                                        component={React.forwardRef(
                                            (props, ref) => (
                                                <RouterLink
                                                    innerRef={ref}
                                                    to="/signup"
                                                    {...props}
                                                />
                                            )
                                        )}
                                        to="/signup"
                                        variant="body2"
                                    >
                                        {"Don't have an account? Sign Up"}
                                    </Link>
                                </Grid>
                            </Grid>
                            <Box mt={5}>{this.Copyright()}</Box>
                        </form>
                    </div>
                </Grid>
            </Grid>
        );
    }
}
SignInSide.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(useStyles)(SignInSide);
